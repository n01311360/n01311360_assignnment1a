﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication1.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%:Title %>.</h2>
  <h2>WELCOME</h2>
    <h3>Book your reservation now</h3>

 First Name: <asp:TextBox runat="server" id="fname"></asp:TextBox>
<asp:RequiredFieldValidator runat="server" id="fnamevalidator" ControlToValidate="fname" errorMessage="**Please enter a First name"> </asp:RequiredFieldValidator>
 <br>
Last Name: <asp:Textbox runat="server" Id="lname"></asp:Textbox>
<asp:RequiredFieldValidator runat="server" id="lnameValidator1" ControlToValidate="fname" errorMessage="**Please enter a Last name"> </asp:RequiredFieldValidator>
   
    <br>
Email: <asp:Textbox runat="server" Id="clientemail"></asp:Textbox>
<asp:RegularExpressionValidator runat="server" ID="emailValidator" ControlToValidate="clientemail" validationgroup="clientemail" ErrorMessage="**Please Provide an email"></asp:RegularExpressionValidator>
  <br />        
Select a time<asp:DropDownList ID="restimes" runat="server">
    <asp:ListItem Enabled="true" Text=""></asp:ListItem>
     <asp:ListItem Text="Breakfast" Value="1"></asp:ListItem>
     <asp:ListItem Text="Brunch" Value="2"></asp:ListItem>
     <asp:ListItem Text="Lunch" Value="4"></asp:ListItem>
     <asp:ListItem Text="Dinner" Value="5"></asp:ListItem>
 </asp:DropDownList>
    <asp:RequiredFieldValidator runat="server" ID="timesvalidator" ControlToValidate="restimes" SetFocusOnError="true" ErrorMessage="You must select a time"></asp:RequiredFieldValidator>
    <br />
    <br/>

    
Number of Guest<asp:CheckBoxList Id="guessamount" runat="server" RepeatDirection="Horizontal">
    <asp:ListItem Text="1-3"></asp:ListItem>
    <asp:listItem Text="4-6"></asp:listItem>
    <asp:listItem Text="6-8"></asp:listItem>
    <asp:listItem Text="10+"></asp:listItem>
    </asp:CheckBoxList>
    <br /> 
    <p>Would you like to signup for special offers?</p>
    <asp:RadioButtonList ID="elist" runat="server" repeatlayout="Flow">
        <asp:ListItem>Email me special offers</asp:ListItem>
        <asp:ListItem>Not Today</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator runat="server" ID="elistvalidator" ControlToValidate="elist" ErrorMessage="**please select"></asp:RequiredFieldValidator>

    <asp:Button id="submit" text=submit runat="server" OnserverClick="submited" />
       
    <asp:ValidationSummary ID="ValidationSummary" runat="server" 
   DisplayMode = "BulletList" ShowSummary = "true" HeaderText="Errors:" />

   

    <br />

</asp:Content>
